import cv2
import numpy as np
from matplotlib import pyplot as plt

def apply_generic_3x3_filter(image, filter_kernel):
    filtered_image = cv2.filter2D(image, -1, filter_kernel)

    return filtered_image

def display_image(window_name, image):
    cv2.imshow(window_name, image)
    cv2.waitKey(0) 
    cv2.destroyAllWindows()

def plot_histogram(image, title):
    if len(image.shape) == 3:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    plt.figure()
    plt.title(title)
    plt.xlabel('Intensity Value')
    plt.ylabel('Count')
    plt.hist(image.ravel(), 256, [0, 256])
    plt.show()

filter_kernel = np.array([
    [1, 0, -1],
    [1, 0, -1],
    [1, 0, -1]
], dtype=np.float32)

image_path = 'analyse/image.png'
image = cv2.imread(image_path)

if image is None:
    print("l'imge ne peut etre chargé")
else:
    filtered_image = apply_generic_3x3_filter(image, filter_kernel)

    cv2.imshow('Image originale', image)
    
    cv2.imshow('Image filtré', filtered_image)
   
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    filtered_image_gray = cv2.cvtColor(filtered_image, cv2.COLOR_BGR2GRAY)

    plt.figure()
    plt.title('Histogramme de limage originale')
    plt.xlabel('Intensité')
    plt.ylabel('cpt')
    plt.hist(image_gray.ravel(), 256, [0, 256])
    plt.show()
    
    plt.figure()
    plt.title('Histogramme de limage filtrée')
    plt.xlabel('Intensité')
    plt.ylabel('Cpt')
    plt.hist(filtered_image_gray.ravel(), 256, [0, 256])
    plt.show()
    
    cv2.destroyAllWindows()
