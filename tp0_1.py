import cv2
import numpy as np
import matplotlib.pyplot as plt
def apply_generic_3x3_filter(image, filter_values):
    rows, cols = image.shape
    filtered_image = np.zeros((rows, cols), dtype=np.float32)
    
    for i in range(1, rows-1):
        for j in range(1, cols-1):
            roi = image[i-1:i+2, j-1:j+2]
            filtered_value = np.sum(roi * filter_values)
            filtered_image[i, j] = max(0, min(255, filtered_value))
    
    return filtered_image.astype(np.uint8)

def normalize_histogram(image):
    histogram, bins = np.histogram(image.flatten(), 256, [0, 256])
    cdf = histogram.cumsum()
    cdf_normalized = cdf * histogram.max() / cdf.max()
    image_normalized = np.interp(image.flatten(), bins[:-1], cdf_normalized).reshape(image.shape)
    return image_normalized.astype(np.uint8)

def stretch_histogram(image, a, b):
    Nmin, Nmax = image.min(), image.max()
    image_stretched = (b - a) * (image - Nmin) / (Nmax - Nmin) + a
    return image_stretched.astype(np.uint8)

def equalize_histogram(image):
    histogram, bins = np.histogram(image.flatten(), 256, [0, 256])
    cdf = histogram.cumsum()
    cdf_normalized = cdf * (2**8 - 1) / cdf.max()
    image_equalized = np.interp(image.flatten(), bins[:-1], cdf_normalized).reshape(image.shape)
    return image_equalized.astype(np.uint8)

def plot_histogram(image, title):
    plt.figure()
    plt.hist(image.flatten(), 256, [0, 256], color='r')
    plt.xlim([0, 256])
    plt.title(title)
    plt.show()

def apply_filter_and_normalize_with_plots(image_path, filter_values, a=0, b=255):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    filtered_image = apply_generic_3x3_filter(image, filter_values)
    normalized_image = normalize_histogram(filtered_image)
    stretched_image = stretch_histogram(normalized_image, a, b)
    equalized_image = equalize_histogram(stretched_image)
    
    cv2.imshow('Image originale', image)
    cv2.imshow('Image filtré', filtered_image)
    
    plot_histogram(image, 'Histogramme original')
    plot_histogram(filtered_image, 'Histogramme Filtrer')
    plot_histogram(normalized_image, 'Histogramme Normaliser ')
    plot_histogram(stretched_image, 'Histogramme etiré ')
    plot_histogram(equalized_image, 'Histogramme égalisé ')
    
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# exemple extreme sample_filter = np.array([[4, -1, 0], [-1, 5, -1], [0, -1, 0]])
sample_filter = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])

apply_filter_and_normalize_with_plots('analyse/image.png', sample_filter)