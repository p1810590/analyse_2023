# analyse_2023
Lien git : https://forge.univ-lyon1.fr/p1810590/analyse_2023.git ,
Langage : python
L’archive contient deux fichiers, le fichier tp0.py qui utilise cv2.filter et le fichier tp0_1.py qui utilise 
l’algorithme du cours pour filtrer selon une matrice donnée.
Lorsque l’on lance le fichier tp0_1.py, on obtient deux fenêtres avec l’image originale et sa version 
modifiée par le filtre ainsi qu’une fenêtre affichant l’histogramme de l’image original.
Lorsque l’on ferme l’histogramme un nouveau apparait qui est l’histogramme de l’image filtrée, puis 
si on ferme cette fenêtre on a l’histogramme normalisé, puis l’étirement et enfin l’égalisation.

![Alt text](image-1.png)
![Alt text](image-2.png)
![Alt text](image-3.png)

Voici ce que donne la fonction avec des valeurs exagérées :
![Alt text](image-4.png)
![Alt text](image-5.png)